<?php
/**
 * Actividades por proyecto
 */

require 'includes/checkauth.php';
require 'includes/config.php';
require 'includes/functions.php';

if ($_SESSION['user']['permissions'] != 1) {
	include 'includes/403.php';
	exit();
}

// Obtener datos del equipo
$sql = "SELECT T.* FROM team_member AS TM JOIN team AS T ON TM.teamId = T.id WHERE TM.userId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_SESSION['user']['id']]);
$team = $stmt->fetch();

// Obtener datos del proyecto
$sql = "SELECT * FROM project AS P JOIN project_team AS PT ON PT.projectId = P.id WHERE P.id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['project']]);
$project = $stmt->fetch();

// Revisar que sea proporcionado un id de proyecto
if (empty($_GET['project'])) {
	include 'includes/404.php';
	exit();
} else {
	// Verificar que el proyecto exista
	$sql = "SELECT * FROM project AS P JOIN project_team AS PT ON PT.projectId = P.id WHERE PT.teamId = ? AND PT.projectId = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$team['id'], $_GET['project']]);

	$result = $stmt->fetch();

	if ($result == false) {
		include 'includes/404.php';
		exit();
	}
}

// Procesar acciones
if (!empty($_GET['action'])) {
	switch ($_GET['action']) {
	case 'assign_activity':

		if (isset($_POST['submit'])) {
			$valid = true;

			if (empty($_POST['memberId'])) {
				$valid = false;
			}

			if (empty($_POST['activityId'])) {
				$valid = false;
			}

			if ($valid) {
				// Revisar si la actividad ya fue asignada anteriormente
				$sql = "SELECT * FROM activity_member WHERE memberId = ? AND activityId = ?";
				$stmt = $pdo->prepare($sql);
				$stmt->execute([$_POST['memberId'], $_POST['activityId']]);
				$previously_assigned = $stmt->fetch();

				if (!$previously_assigned) {
					$sql = "INSERT INTO activity_member(memberId, activityId) VALUES (?, ?)";
					$stmt = $pdo->prepare($sql);
					$result = $stmt->execute([$_POST['memberId'], $_POST['activityId']]);

					if ($result) {
						$_SESSION['message']['type'] = 'success';
						$_SESSION['message']['content'] = 'Actividad asignada correctamente.';
						header('Location: activities.php?project=' . escape($_GET['project']));
						exit();
					} else {
						$_SESSION['message']['type'] = 'danger';
						$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
					}
				}
			} else {
				$_SESSION['message']['type'] = 'danger';
				$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
			}
		}

		break;
	case 'upload_file':
		if (isset($_POST['submit'])) {
			$valid = true;

			if (!isset($_FILES['file'])) {
				$valid = false;
			} else {
				if ($_FILES['file']['error'] !== UPLOAD_ERR_OK) {
					$valid = false;
				}
			}

			if ($valid) {
				$file_extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$file_name = "{$project['name']}_{$team['id']}_" . time() . '.' . $file_extension;
				$file_path = UPLOADS_FOLDER . $file_name;

				if (move_uploaded_file($_FILES['file']['tmp_name'], $file_path)) {
					$sql = "UPDATE project_team SET finalFile = ? WHERE projectId = ? AND teamId = ?";
					$stmt = $pdo->prepare($sql);
					$result = $stmt->execute([$file_name, $project['id'], $team['id']]);

					if ($result) {
						$_SESSION['message']['type'] = 'success';
						$_SESSION['message']['content'] = 'Archivo subido correctamente.';
						header('Location: activities.php?project=' . escape($_GET['project']));
						exit();
					} else {
						$_SESSION['message']['type'] = 'danger';
						$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
					}
				} else {
					$_SESSION['message']['type'] = 'danger';
					$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
				}
			}

		}
		break;
	case 'set_status':
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$valid = true;

			if (empty($_POST['id'])) {
				$valid = false;
			}

			if (!isset($_POST['status'])) {
				$valid = false;
			}

			if ($valid) {
				$sql = "UPDATE activity_member SET status = ? WHERE id = ?";
				$stmt = $pdo->prepare($sql);
				$result = $stmt->execute([$_POST['status'], $_POST['id']]);

				if ($result) {
					echo json_encode(['success' => true]);
					exit();
				} else {
					echo json_encode(['success' => false]);
					exit();
				}
			} else {
				echo json_encode(['success' => false]);
				exit();
			}
		}
		break;
	default:
		break;
	}
}

// Obtener actividades asignadas
$sql = "SELECT AM.id, A.id AS activityId, A.name AS activityName, AM.status, CONCAT(U.firstName, ' ', U.lastName) AS user FROM activity AS A LEFT JOIN activity_member AS AM ON AM.activityId = A.id LEFT JOIN user AS U ON AM.memberId = U.id WHERE A.projectId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['project']]);
$assigned_activities = $stmt->fetchAll();

// Obtener actividades del proyecto
$sql = "SELECT A.id, A.name FROM activity AS A WHERE A.projectId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['project']]);
$activities = $stmt->fetchAll();

// Obtener miembros del proyecto
$sql = "SELECT U.* FROM team_member AS TM JOIN user AS U ON TM.userId = U.id WHERE TM.teamId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$team['id']]);
$team_members = $stmt->fetchAll();

$page_title = "Actividades - " . $project['name'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<style>
	.inlineForm {
		display: flex;
		align-items: center;
		margin-bottom: 1rem;
	}

	.inlineForm>* {
		margin: 0;
	}

	.inlineForm>*:not(:last-child) {
		margin-right: 1rem;
	}
</style>
<main class="page-content">
	<h1>Proyecto: <?php echo $project['name'] ?></h1>

	<?php /*----------  Mensajes  ----------*/?>
	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
	<?php unset($_SESSION['message'])?>
	<?php endif?>
	<h3>Archivo Final:</h3>
	<table class="table">
		<thead class="table-thead">
			<tr>
				<th>Archivo</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?php echo $project['finalFile'] ? "<a href='" . UPLOADS_FOLDER . $project['finalFile'] . "' class='link' download>Descargar</a>" : 'No se ha subido el archivo final' ?></td>
				<td>
					<form enctype="multipart/form-data" action="activities.php?project=<?php echo escape($_GET['project']) ?>&action=upload_file" method="POST">
						<input type="file" name="file" id="file" accept="application/pdf" required>
						<input type="submit" name="submit" value="Subir" class="button button--secondary">
					</form>
				</td>
			</tr>
		</tbody>
	</table>

	<h3>Actividades:</h3>
	<form class="inlineForm" action="activities.php?project=<?php echo escape($_GET['project']) ?>&action=assign_activity" method="POST">
			<select name="memberId" id="user" class="select" required>
				<?php if ($team_members): ?>
					<?php foreach ($team_members as $row): ?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['firstName'] . ' ' . $row['lastName'] ?></option>
					<?php endforeach?>
				<?php else: ?>
						<option selected disabled>No se encontraron resultados</option>
				<?php endif?>
			</select>
			<select name="activityId" id="activity" class="select" required>
				<?php if ($activities): ?>
					<?php foreach ($activities as $row): ?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php endforeach?>
				<?php else: ?>
						<option selected disabled>No se encontraron resultados</option>
				<?php endif?>
			</select>

			<input class="button button--secondary" name="submit" type="submit" value="Asignar">
		</form>
	<table class="table">
		<thead class="table-thead">
			<tr>
				<th>Nombre</th>
				<th>Asignada a</th>
				<th>Aprobada</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($assigned_activities): ?>
				<?php foreach ($assigned_activities as $row): ?>
					<tr>
						<td><?php echo $row['activityName'] ?></td>
						<td><?php echo $row['user'] ?? 'Nadie' ?></td>
						<td>
							<label class="switch">
							  <input data-id="<?php echo $row['id'] ?>" class="status" type="checkbox" <?php echo $row['status'] == 1 ? 'checked' : '' ?>>
							  <span class="slider"></span>
							</label>
						</td>
						<td>
							<a href="revisions.php?id=<?php echo $row['id'] ?>" class="link">Ver avances</a>
						</td>
					</tr>
				<?php endforeach?>
			<?php else: ?>
				<tr>
					<td colspan="4" class="text--center">No se encontraron resultados.</td>
				</tr>
			<?php endif?>
		</tbody>
	</table>
</main>
<?php include 'includes/footer.php'?>
<script>
	$('.switch .status').on('change', function (e) {
		let id = $(this).data('id');

		let status = this.checked ? 1 : 0;

		$.post('activities.php?project=<?php echo $project['id'] ?>&action=set_status', {
			id: id,
			status: status
		}, function (data) {
			if (!data.success) {
				alert('Ha ocurrido un error.');
			}
		}, "json");

	})
</script>
</body>
</html>