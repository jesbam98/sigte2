<?php
/**
 * Crear un nuevo proyecto
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Título de la página
$page_title = 'Nuevo proyecto';

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que el nombre sea enviado
	if (empty($_POST['name'])) {
		$valid = false;
		$errors[] = 'El nombre es obligatorio.';
	}

	/*----------  Verficar que todas la validaciones sean correctas  ----------*/
	if ($valid) {

		// Definir datos del nuevo proyecto
		$new_project = [];
		$new_project[':name'] = $_POST['name'];

		// Obtener la descripción en caso de que sea proporcionada
		$new_project[':description'] = $_POST['description'] ?? '';

		// Guardar el proyecto
		$sql = "INSERT INTO `project` (`name`, `description`) VALUES (:name, :description)";
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute($new_project);

		if ($result) {
			// Obtener el id del proyecto guardado
			$project_id = $pdo->lastInsertId();

			// Verificar si fueron enviadas actividades
			if (!empty($_POST['activities'] && is_array($_POST['activities']))) {

				// Comprobar cada una de las actividades
				foreach ($_POST['activities'] as $activity) {

					if (!empty($activity['name'])) {
						// Definir datos de la actividad
						$new_activity = [];
						$new_activity[':name'] = $activity['name'];
						$new_activity[':description'] = $activity['description'] ?? '';
						$new_activity[':projectId'] = $project_id;

						// Guardar la actividad
						$sql = "INSERT INTO `activity`(`name`, `description`, `projectId`) VALUES (:name, :description, :projectId)";
						$stmt = $pdo->prepare($sql);
						$stmt->execute($new_activity);
					}
				}
			}

			$_SESSION['message']['type'] = 'success';
			$_SESSION['message']['content'] = 'Proyecto registrado correctamente.';
			header('Location: add_project.php');
			exit();
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
		}
	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
	<style>
		.addActivity {
			display: flex;
			align-items: center;
			margin-bottom: 1rem;
		}

		.addActivity>* {
			margin: 0;
		}

		.addActivity>*:not(:last-child) {
			margin-right: 1rem;
		}

		.activityForm {
			display: flex;
			align-items: center;
			margin-bottom: 1rem;
		}

		.activityForm>*{
			margin: 0;
		}

		.activityForm>*:not(:last-child) {
			margin-right: 1rem;
		}
	</style>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Registrar nuevo proyecto</h1>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>

	<?php if (!empty($errors)): ?>
		<div class="alert danger">
			<span class="closebtn">&times;</span>
			<ul>
				<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
				<?php endforeach?>
			</ul>
		</div>
	<?php endif?>

	<form class="form" method="POST">
		<label for="name" class="label">Nombre:</label>
		<input name="name" id="name" type="text" class="input" value="<?php echo $_POST['name'] ?? '' ?>" autofocus required>

		<label for="description" class="label">Descripción:</label>
		<textarea name="description" id="description" class="textarea"></textarea>


		<div class="addActivity">
			<h3>Actividades</h3>

			<button id="addActivity" class="button button--secondary" type="button"><i class="fas fa-plus"></i></button>
		</div>

        <div id="activities"></div>

		<input name="submit" type="submit" value="Guardar" class="button button--primary">
		<input type="reset" value="Cancelar" class="button">
	</form>
</main>
<?php include 'includes/footer.php';?>
<script src="assets/js/project.js"></script>
</body>
</html>