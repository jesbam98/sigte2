<?php
/**
 * Crear una nueva revisión
 */

require 'includes/checkauth.php';
require 'includes/config.php';
require 'includes/functions.php';

// Revisar que sea proporcionado un id de actividad asignada
if (empty($_GET['id'])) {
	include 'includes/404.php';
	exit();
} else {
	// Verificar que la actividad exista
	$sql = "SELECT id FROM activity_member WHERE memberId = ? AND id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_SESSION['user']['id'], $_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		include 'includes/404.php';
		exit();
	}
}

// Título de la página
$page_title = 'Nueva revisión';

// Obtener datos de la actividad asignada
$sql = "SELECT A.*, AM.* FROM activity_member AS AM JOIN activity AS A ON A.id = AM.activityId WHERE AM.memberId = ? AND AM.id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_SESSION['user']['id'], $_GET['id']]);
$assigned_activity = $stmt->fetch();

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que el nombre sea enviado
	if (empty($_POST['name'])) {
		$valid = false;
		$errors[] = 'El nombre es obligatorio.';
	}

	if (!isset($_FILES['file'])) {
		$valid = false;
		$errors[] = 'El archivo es obligatorio.';
	} else {
		if ($_FILES['file']['error'] !== UPLOAD_ERR_OK) {
			$valid = false;
			$errors = 'No se pudo cargar el archivo.';
		}
	}

	/*----------  Verficar que todas las validaciones sean correctas  ----------*/
	if ($valid) {

		$file_extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$file_name = "{$assigned_activity['name']}_{$assigned_activity['memberId']}_" . time() . '.' . $file_extension;
		$file_path = UPLOADS_FOLDER . $file_name;

		if (move_uploaded_file($_FILES['file']['tmp_name'], $file_path)) {

			// Definir datos de la nueva revisión
			$new_revision = [];
			$new_revision[] = $assigned_activity['id'];
			$new_revision[] = $_POST['name'];
			$new_revision[] = $_POST['description'] ?? '';
			$new_revision[] = $file_name;

			// Guardar la revisión
			$sql = "INSERT INTO revision (activityMemberId, name, description, file) VALUES (?, ?, ?, ?)";
			$stmt = $pdo->prepare($sql);
			$result = $stmt->execute($new_revision);

			if ($result) {
				$_SESSION['message']['type'] = 'success';
				$_SESSION['message']['content'] = 'Revisión registrada correctamente.';
				header('Location: add_revision.php?id=' . escape($_GET['id']));
				exit();
			} else {
				$_SESSION['message']['type'] = 'danger';
				$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
			}
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
		}

	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Nueva revisión</h1>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>

	<?php if (!empty($errors)): ?>
		<div class="alert danger">
			<span class="closebtn">&times;</span>
			<ul>
				<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
				<?php endforeach?>
			</ul>
		</div>
	<?php endif?>

	<form class="form" action="" method="POST" enctype="multipart/form-data">
		<label for="name" class="label">Nombre:</label>
		<input name="name" id="name" type="text" class="input" value="<?php echo $_POST['name'] ?? '' ?>" autofocus required>


		<label for="description" class="label">Descripción:</label>
		<input name="description" id="description" type="text" class="input" value="<?php echo $_POST['description'] ?? '' ?>">

		<label for="file" class="label">Archivo:</label>
		<input type="file" name="file" id="file" required>
		<br>
		<br>

		<input name="submit" type="submit" value="Guardar" class="button button--primary">
		<input type="reset" value="Cancelar" class="button">
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>