<?php
/**
 * Crear un nuevo equipo
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Título de la página
$page_title = 'Nuevo equipo';

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que el nombre sea enviado
	if (empty($_POST['name'])) {
		$valid = false;
		$errors[] = 'El nombre es obligatorio.';
	}

	/*----------  Verficar que todas las validaciones sean correctas  ----------*/
	if ($valid) {

		// Definir datos del nuevo equipo
		$new_team = [];
		$new_team[] = $_POST['name'];

		// Guardar el equipo
		$sql = "INSERT INTO `team` (`name`) VALUES (?)";
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute($new_team);

		if ($result) {
			$_SESSION['message']['type'] = 'success';
			$_SESSION['message']['content'] = 'Equipo registrado correctamente.';
			header('Location: add_team.php');
			exit();
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
		}
	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Registrar nuevo equipo</h1>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>

	<?php if (!empty($errors)): ?>
		<div class="alert danger">
			<span class="closebtn">&times;</span>
			<ul>
				<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
				<?php endforeach?>
			</ul>
		</div>
	<?php endif?>

	<form class="form" method="POST">
		<label for="name" class="label">Nombre:</label>
		<input name="name" id="name" type="text" class="input" value="<?php echo $_POST['name'] ?? '' ?>" autofocus required>

		<input name="submit" type="submit" value="Guardar" class="button button--primary">
		<input type="reset" value="Cancelar" class="button">
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>