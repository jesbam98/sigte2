<?php
/**
 * Crear un nuevo usuario
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Título de la página
$page_title = 'Nuevo usuario';

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que el nombre sea enviado
	if (empty($_POST['firstName'])) {
		$valid = false;
		$errors[] = 'El nombre es obligatorio.';
	}

	// Verificar que los apellidos sean enviados
	if (empty($_POST['lastName'])) {
		$valid = false;
		$errors[] = 'Los apellidos son obligatorios.';
	}

	// Verificar que el correo sea enviado
	if (empty($_POST['email'])) {
		$valid = false;
		$errors[] = 'El correo es obligatorio.';
	} else {
		// Revisar que el correo no exista
		$sql = "SELECT id FROM user WHERE email = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$_POST['email']]);

		$result = $stmt->fetch();
		if ($result) {
			$valid = false;
			$errors[] = 'El correo ya está en uso.';
		}
	}

	// Verificar que el usuario sea enviado
	if (empty($_POST['username'])) {
		$valid = false;
		$errors[] = 'El usuario es obligatorio.';
	} else {
		// Revisar que el usuario no exista
		$sql = "SELECT id FROM user WHERE username = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$_POST['username']]);

		$result = $stmt->fetch();
		if ($result) {
			$valid = false;
			$errors[] = 'El usuario ya está en uso.';
		}
	}

	// Verificar que la contraseña sea enviada
	if (empty($_POST['password'])) {
		$valid = false;
		$errors[] = 'La contraseña es obligatoria.';
	} else {

		// Verificar que la contraseña sea mayor o igual a 6 caracteres
		if (strlen($_POST['password']) < 6) {
			$valid = false;
			$errors[] = 'La contraseña debe ser mayor o igual a 6 caracteres.';
		}
	}

	/*----------  Verficar que todas las validaciones sean correctas  ----------*/
	if ($valid) {

		// Definir datos del nuevo usuario
		$new_user = [];
		$new_user[] = $_POST['firstName'];
		$new_user[] = $_POST['lastName'];
		$new_user[] = $_POST['username'];
		$new_user[] = $_POST['email'];
		// Encriptar contraseña
		$new_user[] = password_hash($_POST['password'], PASSWORD_DEFAULT);

		// Guardar el equipo
		$sql = "INSERT INTO `user` (`firstName`, `lastName`, `username`, `email`, `password`, `permissions`) VALUES (?, ?, ?, ?, ?, 0)";
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute($new_user);

		if ($result) {
			$_SESSION['message']['type'] = 'success';
			$_SESSION['message']['content'] = 'Usuario registrado correctamente.';
			header('Location: add_user.php');
			exit();
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
		}
	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Agregar usuario</h1>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>

	<?php if (!empty($errors)): ?>
		<div class="alert danger">
			<span class="closebtn">&times;</span>
			<ul>
				<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
				<?php endforeach?>
			</ul>
		</div>
	<?php endif?>

	<form class="form" method="POST">
		<label for="firstName" class="label">Nombre:</label>
		<input name="firstName" id="firstName" type="text" class="input" value="<?php echo $_POST['firstName'] ?? '' ?>" autofocus required>

		<label for="lastName" class="label">Apellidos:</label>
		<input name="lastName" id="lastName" type="text" class="input" value="<?php echo $_POST['lastName'] ?? '' ?>" required>

		<label for="email" id="email" class="label">Correo:</label>
		<input name="email" id="email" type="email" class="input" value="<?php echo $_POST['email'] ?? '' ?>" required>

		<label for="username" class="label">Usuario:</label>
		<input name="username" id="username" type="text" class="input" value="<?php echo $_POST['username'] ?? '' ?>" required>

		<label for="password" class="label">Contraseña:</label>
		<input name="password" id="password" type="password" class="input" required>

		<input name="submit" type="submit" value="Guardar" class="button button--primary">
		<input type="reset" value="Cancelar" class="button">
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>