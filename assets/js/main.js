var modalTriggers;
var closeAlertBtns;

function initModalTriggers() {
    modalTriggers = document.querySelectorAll('[data-toggle=modal][data-target]');

    modalTriggers.forEach(trigger => {
        let targetModal = document.querySelector(trigger.dataset.target);

        if (targetModal) {
            targetModal.addEventListener('click', function (e) {
                if (e.target == targetModal) {
                    targetModal.classList.remove('modal--active');
                }

            });
        }
        trigger.addEventListener('click', function () {
            toggleModal(trigger.dataset.target);
        });
    });
}

function toggleModal(modal) {
    let targetModal = document.querySelector(modal);
    if (targetModal) {
        targetModal.classList.toggle('modal--active');
    }
}

function initAlerts() {
    closeAlertBtns = document.getElementsByClassName("closebtn");

    for (let i = 0; i < closeAlertBtns.length; i++) {
      closeAlertBtns[i].addEventListener('click',function() {
        var div = this.parentElement;
        div.style.opacity = "0";
        setTimeout(function(){ div.style.display = "none"; }, 600);
      });
    }
}

document.addEventListener('DOMContentLoaded', function () {
    initModalTriggers();
    initAlerts();
});