var addActivityBtn;
var numFields = 0;

function addField() {
    var html = `<div id="tempAct${numFields}" class="activityForm">
    <input name="activities[temp-${numFields}][name]" type="text" class="input" placeholder="Nombre">
    <input name="activities[temp-${numFields}][description]" type="text" class="input" placeholder="Descripción">
    <button data-id="${numFields}" type="button" class="deleteTempField button button--danger" ><i class="fas fa-times"></i></button>
    </div>`;
    $('#activities').append(html);
    numFields++;
}

function removeField(id) {
    $('#tempAct' + id).remove();
    numFields--;
}

function deleteActivity(id) {
    var confirmed = confirm('¿Desea elminar esta actividad?');

    if (confirmed == true) {
        $.post("delete_activity.php", { id: id },
            function (data, textStatus, jqXHR) {
                if (data.success == true) {
                    $('#act' + id).remove();
                } else {
                    alert('Ha ocurrido un error inesperado');
                }
            },
            "json"
        );
    }
}

$(function () {
    addActivityBtn = $('#addActivity');

    addActivityBtn.on('click', function () {
        addField();
    });

    $('#savedActivities').on('click', '.deleteActivity', function () {
        var button = this;
        if (button.getAttribute('data-id')) {
            var id = button.dataset.id;
            deleteActivity(id);
        }
    });

    $('#activities').on('click', '.deleteTempField', function () {
        var button = this;
        if (button.getAttribute('data-id')) {
            var id = button.dataset.id;
            removeField(id);
        }
    });
});