<?php
/**
 * Eliminar actividades
 */
require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	echo json_encode(['error' => 'No tienes acceso a esta página']);
	exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if (!empty($_POST['id'])) {
		$sql = "DELETE FROM activity WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute([$_POST['id']]);

		if ($result) {
			echo json_encode(['success' => true]);
		} else {
			echo json_encode(['success' => false]);
		}
	}
}