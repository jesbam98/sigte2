<?php
/**
 * Eliminar un proyecto
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Revisar que sea proporcionado un id de proyecto
if (empty($_GET['id'])) {
	header('Location: projects.php');
	exit();
} else {
	// Verificar que el proyecto a editar exista
	$sql = "SELECT id FROM project WHERE id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		header('Location: projects.php');
		exit();
	}
}

// Título de la página
$page_title = 'Confirmar eliminación';

// Obtener los datos del proyecto a editar
$sql = "SELECT * FROM project WHERE id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$team = $stmt->fetch();

// Procesar formulario
if (isset($_POST['submit'])) {

	// Verificar que sea enviado el id en el formulario
	if (empty($_POST['id'])) {
		header('Location: teams.php');
		exit();
	}

	if ($_GET['id'] == $_POST['id']) {

		// Eliminar el proyecto
		$sql = "DELETE FROM project WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute([$_POST[id]]);

		if ($result) {
			$_SESSION['message']['type'] = 'success';
			$_SESSION['message']['content'] = 'Proyecto eliminado correctamente';
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema';
		}

	}

	// Redireccionar al listado de usuarios
	header('Location: projects.php');
	exit();
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>¿Está seguro que desea eliminar este proyecto?</h1>

	<form class="form" method="POST">
		<input name="id" type="hidden" value="<?php echo $team['id'] ?>">
		<input name="submit" type="submit" value="Aceptar" class="button button--danger">
		<a href="projects.php" class="button">Cancelar</a>
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>