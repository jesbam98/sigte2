<?php
/**
 * Eliminar un usuario
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Revisar que sea proporcionado un id de usuario
if (empty($_GET['id'])) {
	header('Location: users.php');
	exit();
} else {
	// Verificar que el usuario a editar exista
	$sql = "SELECT id FROM user WHERE id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		header('Location: users.php');
		exit();
	}
}

// Título de la página
$page_title = 'Confirmar eliminación';

// Obtener los datos del usuario a editar
$sql = "SELECT * FROM user WHERE id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$user = $stmt->fetch();

// Procesar formulario
if (isset($_POST['submit'])) {

	// Verificar que sea enviado el id en el formulario
	if (empty($_POST['id'])) {
		header('Location: users.php');
		exit();
	}

	if ($_GET['id'] == $_POST['id']) {

		// Eliminar al usuario
		$sql = "DELETE FROM user WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$result = $stmt->execute([$_POST[id]]);

		if ($result) {
			$_SESSION['message']['type'] = 'success';
			$_SESSION['message']['content'] = 'Usuario eliminado correctamente';
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema';
		}

	}

	// Redireccionar al listado de usuarios
	header('Location: users.php');
	exit();
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>¿Está seguro que desea eliminar este usuario?</h1>

	<form class="form" method="POST">
		<input name="id" type="hidden" value="<?php echo $user['id'] ?>">
		<input name="submit" type="submit" value="Aceptar" class="button button--danger">
		<a href="users.php" class="button">Cancelar</a>
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>