<?php
/**
 * Editar un proyecto
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Revisar que sea proporcionado un id de proyecto
if (empty($_GET['id'])) {
	header('Location: projects.php');
	exit();
} else {
	// Verificar que el proyecto a editar exista
	$sql = "SELECT id FROM project WHERE id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		header('Location: projects.php');
		exit();
	}
}

// Título de la página
$page_title = 'Editar proyecto';

// Obtener datos del proyecto
$sql = "SELECT * FROM project WHERE id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$project = $stmt->fetch();

// Obtener actividades ligadas al proyecto
$sql = "SELECT * FROM activity WHERE projectId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$activities = $stmt->fetchAll();

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que el nombre sea enviado
	if (empty($_POST['name'])) {
		$valid = false;
		$errors[] = 'El nombre es obligatorio.';
	}

	/*----------  Verficar que todas las validaciones sean correctas  ----------*/
	if ($valid) {

		// Definir datos del proyecto editado
		$edited_project = [];
		$edited_project[':name'] = $_POST['name'];
		$edited_project[':id'] = $_GET['id'];

		// Obtener la descripción en caso de que sea proporcionada
		$edited_project[':description'] = $_POST['description'] ?? '';

		$sql = "UPDATE `project` SET `name`= :name,`description`= :description WHERE id = :id";
		$stmt = $pdo->prepare($sql);
		$stmt->execute($edited_project);

		// Verificar si fueron enviadas actividades
		if (!empty($_POST['activities'] && is_array($_POST['activities']))) {

			// Comprobar cada una de las actividades
			foreach ($_POST['activities'] as $activity) {

				if (!empty($activity['id'])) {

					// Verificar que la actividad a editar exista
					$sql = "SELECT id FROM activity WHERE id = ? AND projectId = ?";
					$stmt = $pdo->prepare($sql);
					$stmt->execute([$activity['id'], $project['id']]);
					$result = $stmt->fetch();

					if ($result) {
						$edited_activity = [];
						$edited_activity[':id'] = $activity['id'];
						$edited_activity[':name'] = $activity['name'];
						$edited_activity[':description'] = $activity['description'] ?? '';

						$sql = "UPDATE `activity` SET `name`= :name,`description`= :description WHERE id = :id";
						$stmt = $pdo->prepare($sql);
						$stmt->execute($edited_activity);
					}

				} else {
					if (!empty($activity['name'])) {
						// Definir datos de la actividad
						$new_activity = [];
						$new_activity[':name'] = $activity['name'];
						$new_activity[':description'] = $activity['description'] ?? '';
						$new_activity[':projectId'] = $project['id'];

						$sql = "INSERT INTO `activity`(`name`, `description`, `projectId`) VALUES (:name, :description, :projectId)";

						$stmt = $pdo->prepare($sql);
						$stmt->execute($new_activity);
					}
				}
			}
		}

		$_SESSION['message']['type'] = 'success';
		$_SESSION['message']['content'] = 'Proyecto actualizado correctamente.';
		header('Location: edit_project.php?id=' . $_GET['id']);
		exit();
	} else {
		$_SESSION['message']['type'] = 'danger';
		$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
	<style>
		.addActivity {
			display: flex;
			align-items: center;
			margin-bottom: 1rem;
		}

		.addActivity>* {
			margin: 0;
		}

		.addActivity>*:not(:last-child) {
			margin-right: 1rem;
		}

		.activityForm {
			display: flex;
			align-items: center;
			margin-bottom: 1rem;
		}

		.activityForm>*{
			margin: 0;
		}

		.activityForm>*:not(:last-child) {
			margin-right: 1rem;
		}
	</style>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Editar proyecto</h1>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>

	<?php if (!empty($errors)): ?>
		<div class="alert danger">
			<span class="closebtn">&times;</span>
			<ul>
				<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
				<?php endforeach?>
			</ul>
		</div>
	<?php endif?>

	<form class="form" method="POST">
		<label for="name" class="label">Nombre:</label>
		<input name="name" id="name" type="text" class="input" value="<?php echo $project['name'] ?? '' ?>" autofocus required>

		<label for="description" class="label">Descripción:</label>
		<textarea name="description" id="description" class="textarea"><?php echo $project['description'] ?></textarea>


		<div class="addActivity">
			<h3>Actividades</h3>

			<button id="addActivity" class="button button--secondary" type="button"><i class="fas fa-plus"></i></button>
		</div>

		<?php if ($activities): ?>
		<div id="savedActivities">
			<?php foreach ($activities as $row): ?>
				<div id="act<?php echo $row['id'] ?>" class="activityForm">
                    <input type="hidden" name="activities[<?php echo $row['id'] ?>][id]" value="<?php echo $row['id'] ?>">
                    <input name="activities[<?php echo $row['id'] ?>][name]" type="text" class="input" placeholder="Nombre" value="<?php echo $row['name'] ?>">
                    <input name="activities[<?php echo $row['id'] ?>][description]" type="text" class="input" placeholder="Descripción" value="<?php echo $row['description'] ?>">
                    <button data-id="<?php echo $row['id'] ?>" type="button" class="deleteActivity button button--danger"><i class="fas fa-times"></i></button>
                </div>
			<?php endforeach?>
		</div>
		<?php endif?>

        <div id="activities"></div>

		<input name="submit" type="submit" value="Guardar" class="button button--primary">
		<input type="reset" value="Cancelar" class="button">
	</form>
</main>
<?php include 'includes/footer.php';?>
<script src="assets/js/project.js"></script>
</body>
</html>