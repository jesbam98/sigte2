<?php
/**
 * Editar equipo
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Revisar que sea proporcionado un id de equipo
if (empty($_GET['id'])) {
	header('Location: teams.php');
	exit();
} else {
	// Verificar que el equipo a editar exista
	$sql = "SELECT id FROM team WHERE id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		header('Location: teams.php');
		exit();
	}
}

// Título de la página
$page_title = 'Editar equipo';

// Obtener los datos del equipo a editar
$sql = "SELECT * FROM team WHERE id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$team = $stmt->fetch();

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que el nombre sea enviado
	if (empty($_POST['name'])) {
		$valid = false;
		$errors[] = 'El nombre es obligatorio.';
	}

	/*----------  Verficar que todas las validaciones sean correctas  ----------*/
	if ($valid) {

		// Definir datos del equipo editado
		$edited_team = [];
		$edited_team[':id'] = $_GET['id'];
		$edited_team[':name'] = $_POST['name'];

		$sql = "UPDATE `team` SET `name` = :name WHERE id = :id";
		$stmt = $pdo->prepare($sql);

		$result = $stmt->execute($edited_team);

		if ($result) {
			$_SESSION['message']['type'] = 'success';
			$_SESSION['message']['content'] = 'Equipo actualizado correctamente.';
			header('Location: edit_team.php?id' . $_GET['id']);
			exit();
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
		}
	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Editar equipo</h1>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>

	<?php if (!empty($errors)): ?>
		<div class="alert danger">
			<span class="closebtn">&times;</span>
			<ul>
				<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
				<?php endforeach?>
			</ul>
		</div>
	<?php endif?>

	<form class="form" method="POST">
		<label for="name" class="label">Nombre:</label>
		<input name="name" id="name" type="text" class="input" value="<?php echo $team['name'] ?? '' ?>" autofocus required>

		<input name="submit" type="submit" value="Guardar" class="button button--primary">
		<input type="reset" value="Cancelar" class="button">
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>