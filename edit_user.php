<?php
/**
 * Editar un usuario
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Revisar que sea proporcionado un id de usuario
if (empty($_GET['id'])) {
	header('Location: users.php');
	exit();
} else {
	// Verificar que el usuario a editar exista
	$sql = "SELECT id FROM user WHERE id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		header('Location: users.php');
		exit();
	}
}

// Título de la página
$page_title = 'Editar usuario';

// Obtener los datos del usuario a editar
$sql = "SELECT * FROM user WHERE id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$user = $stmt->fetch();

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que el nombre sea enviado
	if (empty($_POST['firstName'])) {
		$valid = false;
		$errors[] = 'El nombre es obligatorio.';
	}

	// Verificar que los apellidos sean enviados
	if (empty($_POST['lastName'])) {
		$valid = false;
		$errors[] = 'Los apellidos son obligatorios.';
	}

	// Verificar que el correo sea enviado
	if (empty($_POST['email'])) {
		$valid = false;
		$errors[] = 'El correo es obligatorio.';
	} else {
		// Revisar que el correo no exista
		$sql = "SELECT id FROM user WHERE email = ? AND email != ?";
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$_POST['email'], $user['email']]);

		$result = $stmt->fetch();
		if ($result) {
			$valid = false;
			$errors[] = 'El correo ya está en uso.';
		}
	}

	// Verificar que el usuario sea enviado
	if (empty($_POST['username'])) {
		$valid = false;
		$errors[] = 'El usuario es obligatorio.';
	} else {
		// Revisar que el usuario no exista
		$sql = "SELECT id FROM user WHERE username = ? AND username != ?";
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$_POST['username'], $user['username']]);

		$result = $stmt->fetch();
		if ($result) {
			$valid = false;
			$errors[] = 'El usuario ya está en uso.';
		}
	}

	// Verificar que la contraseña sea enviada
	if (!empty($_POST['password'])) {

		// Verificar que la contraseña sea mayor o igual a 6 caracteres
		if (strlen($_POST['password']) < 6) {
			$valid = false;
			$errors[] = 'La contraseña debe ser mayor o igual a 6 caracteres.';
		}
	}

	/*----------  Verficar que todas las validaciones sean correctas  ----------*/
	if ($valid) {

		// Definir datos del nuevo usuario
		$edited_user = [];
		$edited_user[':firstName'] = $_POST['firstName'];
		$edited_user[':lastName'] = $_POST['lastName'];
		$edited_user[':username'] = $_POST['username'];
		$edited_user[':email'] = $_POST['email'];

		if (!empty($_POST['password'])) {
			// Encriptar contraseña
			$edited_user[':password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
		}

		$edited_user[':id'] = $_GET['id'];

		$sql = "UPDATE `user` SET `firstName`= :firstName,`lastName`= :lastName,`username`= :username,`email`= :email";

		// En caso de que la contraseña sea proporcionada se debe alterar la consulta
		if (!empty($_POST['password'])) {
			$sql .= ",`password`= :password";
		}

		$sql .= " WHERE id = :id";

		$stmt = $pdo->prepare($sql);

		$result = $stmt->execute($edited_user);

		if ($result) {
			$_SESSION['message']['type'] = 'success';
			$_SESSION['message']['content'] = 'Usuario modificado correctamente.';
			header('Location: edit_user.php?id=' . $_GET['id']);
			exit();
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
		}
	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Editar usuario</h1>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>

	<?php if (!empty($errors)): ?>
		<div class="alert danger">
			<span class="closebtn">&times;</span>
			<ul>
				<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
				<?php endforeach?>
			</ul>
		</div>
	<?php endif?>

	<form class="form" method="POST">
		<label for="firstName" class="label">Nombre:</label>
		<input name="firstName" id="firstName" type="text" class="input" value="<?php echo $user['firstName'] ?? '' ?>" autofocus required>

		<label for="lastName" class="label">Apellidos:</label>
		<input name="lastName" id="lastName" type="text" class="input" value="<?php echo $user['lastName'] ?? '' ?>" required>

		<label for="email" id="email" class="label">Correo:</label>
		<input name="email" id="email" type="email" class="input" value="<?php echo $user['email'] ?? '' ?>" required>

		<label for="username" class="label">Usuario:</label>
		<input name="username" id="username" type="text" class="input" value="<?php echo $user['username'] ?? '' ?>" required>

		<label for="password" class="label">Contraseña:</label>
		<input name="password" id="password" type="password" class="input">

		<input name="submit" type="submit" value="Actualizar" class="button button--primary">
		<input type="reset" value="Cancelar" class="button">
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>