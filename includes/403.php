<?php
/**
 * Error 403
 */
http_response_code(403);
?>
<!DOCTYPE html>
<html lang="ea">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="assets/css/style.css">
	<title>Error 403</title>
</head>
<body class="page">
	<main class="page-content">
		<h1>No tienes acceso a esta página.</h1>
	</main>
</body>
</html>