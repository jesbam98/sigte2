<?php
/**
 * Error 404
 */
http_response_code(404);
?>
<!DOCTYPE html>
<html lang="ea">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="assets/css/style.css">
	<title>Error 404</title>
</head>
<body class="page">
	<main class="page-content">
		<h1>La página que intentas buscar no existe.</h1>
	</main>
</body>
</html>