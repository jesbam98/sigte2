<?php
/**
 * Revisar que el usuario esté autenticado
 */

if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

if (empty($_SESSION['logged_in'])) {
	header('Location: login.php');
	exit();
}