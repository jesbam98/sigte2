<?php
/**
 * Revisar el nivel de permisos del usuario
 */
if (isset($permission_level)) {
	if ($_SESSION['user']['permissions'] != $permission_level) {
		include '403.php';
		exit();
	}
}

?>