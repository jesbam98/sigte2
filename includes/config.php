<?php

/**
 * Configuración del sistema
 */

// Dirección de la base de datos
define('DB_SERVER', 'localhost');
// Usuario de la base de datos
define('DB_USERNAME', 'root');
// Contraseña de la base de datos
define('DB_PASSWORD', '');
// Nombre de la base de datos
define('DB_NAME', 'sigte');
// Establecer la zona horaria
date_default_timezone_set('America/Mexico_City');
// Establecer idioma
setlocale(LC_ALL, 'es.UTF-8');
// Carpeta de subidas
define('UPLOADS_FOLDER', 'uploads/');

/* ----------------- Establecer conexión a la Base de Datos ----------------- */
try {
	$pdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->exec("SET NAMES UTF8");
} catch (PDOException $ex) {
	die("ERROR: No se pudo conectar. " . $ex->getMessage());
}
