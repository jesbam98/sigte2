<?php
/**
 * Funciones globales
 */

/**
 * Escapar datos para evitar ataques XSS
 * @param  string $html Texto a escapar
 * @return string       Texto escapado
 */
function escape($html) {
	return htmlspecialchars($html, ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8");
}
