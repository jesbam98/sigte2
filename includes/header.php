<?php
/**
 * Cabecera HTML
 */
?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="assets/icons/fontawesome/css/all.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<script src="assets/js/jquery.min.js"></script>
<title><?php echo isset($page_title) ? "$page_title | " : '' ?>SIGTE</title>