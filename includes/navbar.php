<?php
/**
 * Barra de navegación
 */
?>
<header>
    <div class="navbar navbar--dark">
        <input type="checkbox" id="navbar__menu-toggle" hidden>
        <div class="navbar__logo">
            <a href="index.php" class="navbar__logo-link">SIGTE</a>
        </div>
        <nav class="navbar__menu">
            <a id="home-link" href="index.php" class="navbar__link">Inicio</a>
            <?php if ($_SESSION['user']['permissions'] == 2): ?>
            <a id="users-link" href="users.php" class="navbar__link">Usuarios</a>
            <a id="projects-link" href="projects.php" class="navbar__link">Proyectos</a>
            <a id="teams-link" href="teams.php" class="navbar__link">Equipos</a>
            <?php else: ?>
            <a id="myprojects-link" href="myprojects.php" class="navbar__link">Mis Proyectos</a>
            <a id="myprojects-link" href="myactivities.php" class="navbar__link">Mis Actividades</a>
            <?php endif?>
        </nav>
        <nav class="navbar__menu navbar__menu--right">
            <div class="navbar__dropdown dropdown">
                <a href="#" class="dropdown__button navbar__link">
                    <?php echo $_SESSION['user']['firstName'] ?>
                </a>
                <div class="dropdown__content">
                    <a href="logout.php" class="navbar__link">Salir</a>
                </div>
            </div>
        </nav>
        <label for="navbar__menu-toggle" class="navbar__menu-button">
            <span class="navbar__menu-icon"></span>
        </label>
    </div>
</header>