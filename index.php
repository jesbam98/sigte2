<?php
require 'includes/checkauth.php';
require 'includes/config.php';

// Título de la página
$page_title = 'Inicio';

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Bienvenido <?php echo $_SESSION['user']['firstName'] ?></h1>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>