<?php
/**
 * Iniciar Sesión
 */

if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

require 'includes/config.php';

// Título de la página
$page_title = 'Iniciar Sesión';

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que se haya enviado un usuario
	if (empty($_POST['username'])) {
		$valid = false;
		$errors[] = "El usuario es obligatorio.";
	}

	// Verificar que se haya enviado una contraseña
	if (empty($_POST['password'])) {
		$valid = false;
		$errors[] = "La contraseña es obligatoria.";
	}

	// Verificar que todo sea válido
	if ($valid) {

		// Obtener los datos del usuario proporcionado
		$sql = "SELECT * FROM user WHERE username = ? LIMIT 1";
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$_POST['username']]);
		$user = $stmt->fetch();

		// Comprobar que el usuario exista
		if ($user) {

			// Comprobar contraseña
			if (password_verify($_POST['password'], $user['password'])) {
				// Almacenar datos del usuario en la sesión
				$_SESSION['logged_in'] = true;
				$_SESSION['user'] = $user;
				header('Location: index.php');
				exit();
			} else {
				$_SESSION['message']['type'] = 'danger';
				$_SESSION['message']['content'] = 'El usuario y/o contraseña son incorrectos.';
			}
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'El usuario y/o contraseña son incorrectos.';
		}
	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
	<style>
		body {
			background-color: var(--primary-color);
		}

		.login {
			width: 100%;
			height: 100%;
			min-height: 100vh;
			display: flex;
			align-items: center;
			justify-content: center;
			padding: 10px;
		}

		.login>form {
			padding: 30px;
			background-color: #fff;
			border-radius: 20px;
		}
	</style>
</head>
<body class="page">
<main class="login">
	<form action="" method="POST">
		<h1 class="text--center">Iniciar Sesión</h1>

		<?php /*----------  Mensajes  ----------*/?>
		<?php if (isset($_SESSION['message'])): ?>
			<div class="alert <?php echo $_SESSION['message']['type'] ?>">
				<span class="closebtn">&times;</span>
				<?php echo $_SESSION['message']['content'] ?>
			</div>
		<?php unset($_SESSION['message'])?>
		<?php endif?>

		<?php /*----------  Errores de Validación  ----------*/?>
		<?php if (!empty($errors)): ?>
			<div class="alert danger">
				<span class="closebtn">&times;</span>
				<ul>
					<?php foreach ($errors as $error): ?>
					<li><?php echo $error ?></li>
					<?php endforeach?>
				</ul>
			</div>
		<?php endif?>

		<label for="username" class="label">Usuario:</label>
		<input name="username" id="username" type="text" class="input">

		<label for="password" class="label">Contraseña:</label>
		<input name="password" id="password" type="password" class="input">

		<input name="submit" type="submit" value="Iniciar Sesión" class="button button--primary button--block">
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>