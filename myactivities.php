<?php
/**
 * Mis actividades
 */

require 'includes/checkauth.php';
require 'includes/config.php';
require 'includes/functions.php';

if ($_SESSION['user']['permissions'] == 2) {
	include 'includes/403.php';
	exit();
}

// Título de la página
$page_title = 'Mis actividades';

// Obtener actividades asignadas
$sql = "SELECT AM.id, A.id AS activityId, A.name AS activityName, AM.status, CONCAT(U.firstName, ' ', U.lastName) AS user FROM activity AS A LEFT JOIN activity_member AS AM ON AM.activityId = A.id LEFT JOIN user AS U ON AM.memberId = U.id WHERE AM.memberId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_SESSION['user']['id']]);
$assigned_activities = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Mis Actividades</h1>

	<table class="table">
		<thead class="table-thead">
			<tr>
				<th>Nombre</th>
				<th>Descripción</th>

			</tr>
		</thead>
		<tbody>
			<?php if ($assigned_activities): ?>
				<?php foreach ($assigned_activities as $row): ?>
					<tr>
						<td><?php echo $row['activityName'] ?></td>
						<td>
							<a href="revisions.php?id=<?php echo $row['id'] ?>" class="link">Ver avances</a>
						</td>
					</tr>
				<?php endforeach?>
			<?php else: ?>
				<tr>
					<td colspan="5" class="text--center">No se encontraron resultados</td>
				</tr>
			<?php endif?>
		</tbody>
	</table>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>