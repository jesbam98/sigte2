<?php
/**
 * Mis Proyectos
 */

require 'includes/checkauth.php';
require 'includes/config.php';
require 'includes/functions.php';

if ($_SESSION['user']['permissions'] == 2) {
	include 'includes/403.php';
	exit();
}

// Título de la página
$page_title = 'Mis Proyectos';

// Obtener los proyectos del usuario
$sql = "SELECT PT.*, P.id AS projectId, P.name AS projectName, P.description AS projectDescription FROM project AS P JOIN project_team AS PT ON PT.projectId = P.id JOIN team_member AS TM ON PT.teamId = TM.teamId WHERE TM.userId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_SESSION['user']['id']]);
$projects = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Mis Proyectos</h1>

	<table class="table">
		<thead class="table-thead">
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Calificación</th>
				<?php if ($_SESSION['user']['permissions'] == 1): ?>
				<th>Acciones</th>
				<?php endif?>
			</tr>
		</thead>
		<tbody>
			<?php if ($projects): ?>
				<?php foreach ($projects as $row): ?>
					<tr>
						<td><?php echo $row['projectId'] ?></td>
						<td><?php echo $row['projectName'] ?></td>
						<td><?php echo $row['projectDescription'] ?></td>
						<td><?php echo $row['score'] ?></td>
						<?php if ($_SESSION['user']['permissions'] == 1): ?>
						<td><a href="activities.php?project=<?php echo escape($row['projectId']) ?>" class="link">Ver actividades</a></td>
						<?php endif?>
					</tr>
				<?php endforeach?>
			<?php else: ?>
				<tr>
					<td colspan="5" class="text--center">No se encontraron resultados</td>
				</tr>
			<?php endif?>
		</tbody>
	</table>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>