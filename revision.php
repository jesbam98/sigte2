<?php
/**
 * Revisión de una actividad asignada
 */

require 'includes/checkauth.php';
require 'includes/config.php';
require 'includes/functions.php';

// Revisar que sea proporcionado un id de revisión
if (empty($_GET['id'])) {
	include 'includes/404.php';
	exit();
} else {

	// Verificar que la revisión exista
	$sql = "SELECT id FROM revision WHERE id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		include 'includes/404.php';
		exit();
	}
}

// Título de la página
$page_title = 'Detalles de revision';

// Obtener datos de la revisión
$sql = "SELECT * FROM revision WHERE id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$revision = $stmt->fetch();

// Procesar formulario
if (!empty($_GET['action'])) {
	$action = $_GET['action'];

	if ($action == 'add_comment') {

		if (isset($_POST['submit'])) {
			$valid = true;

			if (!isset($_POST['comment'])) {
				$valid = false;
			}

			if ($valid) {
				$sql = "INSERT INTO comment(content, revisionId, userId) VALUES (?, ?, ?)";
				$stmt = $pdo->prepare($sql);
				$result = $stmt->execute([$_POST['comment'], $revision['id'], $_SESSION['user']['id']]);
			}
		}

	}
}

// Obtener comentarios
$sql = "SELECT C.*, CONCAT(U.firstName, ' ', U.lastName) AS user FROM comment AS C JOIN user AS U ON C.userId = U.id WHERE C.revisionId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$comments = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
	<style>
		.comments {
			list-style: none;
			margin: 0;
			padding: 0;
			margin-bottom: 20px;
		}

		.comments li {
			padding: 15px;
			margin-top: -1px;
			border: 1px solid #ddd;

		}
	</style>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Revisión: <?php echo $revision['name'] ?></h1>

	<h3>Descripción:</h3>
	<p><?php echo $revision['description'] ?></p>

	<h3>Comentarios:</h3>
	<ul class="comments">
		<?php foreach ($comments as $row): ?>
			<li>
				<strong><?php echo $row['user'] ?></strong> - <?php echo $row['content'] ?>
			</li>
		<?php endforeach?>
	</ul>

	<form action="revision.php?id=<?php echo escape($_GET['id']) ?>&action=add_comment" method="POST">
		<textarea name="comment" id="comment" class="textarea" placeholder="Comentario" required></textarea>
		<input type="submit" name="submit" value="Enviar" class="button button--secondary">
	</form>

</main>
<?php include 'includes/footer.php';?>
</body>
</html>