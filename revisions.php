<?php
/**
 * Revisiones de una actividad asignada
 */

require 'includes/checkauth.php';
require 'includes/config.php';
require 'includes/functions.php';

// Revisar que sea proporcionado un id de actividad asignada
if (empty($_GET['id'])) {
	include 'includes/404.php';
	exit();
} else {
	// Verificar que la actividad exista
	$sql = "SELECT id FROM activity_member WHERE id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		include 'includes/404.php';
		exit();
	}
}

// Título de la página
$page_title = 'Revisiones';

// Obtener datos de la actividad
$sql = "SELECT A.* FROM activity AS A JOIN activity_member AS AM ON AM.activityId = A.id WHERE AM.id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$activity = $stmt->fetch();

// Obtener revisiones
$sql = "SELECT * FROM revision WHERE activityMemberId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$revisions = $stmt->fetchAll();

// Revisar si el usuario puede agregar revisiones
$sql = "SELECT id FROM activity_member WHERE id = ? AND memberId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id'], $_SESSION['user']['id']]);
$canModify = $stmt->fetch();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Actividad: <?php echo $activity['name'] ?></h1>

	<h3>Revisiones:</h3>
	<?php if ($canModify): ?>
	<div class="actionBar">
		<a href="add_revision.php?id=<?php echo escape($_GET['id']) ?>" class="button button--secondary">Nueva revisión</a>
	</div>
	<?php endif?>
	<table class="table">
		<thead class="table-thead">
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Archivo</th>
				<th>Fecha</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($revisions): ?>
				<?php foreach ($revisions as $row): ?>
					<tr>
						<td><?php echo $row['id'] ?></td>
						<td><?php echo $row['name'] ?></td>
						<td><a href="<?php echo UPLOADS_FOLDER . $row['file'] ?>" class="link" download>Descargar</a></td>
						<td><?php echo $row['createdAt'] ?></td>
						<td><a href="revision.php?id=<?php echo escape($row['id']) ?>" class="link">Ver</a></td>
					</tr>
				<?php endforeach?>
			<?php endif?>
		</tbody>
	</table>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>