<?php
/**
 * Calificar
 */

require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Revisar que sea proporcionado un id de proyecto
if (empty($_GET['teamId']) && empty($_GET['projectId'])) {
	include 'includes/404.php';
	exit();
} else {
	// Verificar que el equipo a editar exista
	$sql = "SELECT * FROM project_team WHERE projectId = ? AND teamId = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['projectId'], $_GET['teamId']]);

	$result = $stmt->fetch();

	if ($result == false) {
		include 'includes/404.php';
		exit();
	}
}

// Título de la página
$page_title = 'Editar calificación';

// Obtener los datos de la calificación
$sql = "SELECT * FROM project_team WHERE projectId = ? AND teamId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['projectId'], $_GET['teamId']]);
$project_team = $stmt->fetch();

// Errores de validación
$errors = [];

// Procesar formulario
if (isset($_POST['submit'])) {
	$valid = true;

	// Verificar que la calificación sea enviada
	if (empty($_POST['score'])) {
		$valid = false;
		$errors[] = 'La calificación es obligatoria.';
	}

	/*----------  Verficar que todas las validaciones sean correctas  ----------*/
	if ($valid) {

		// Definir datos del equipo editado
		$updated_score = [];
		$updated_score[] = $_POST['score'];
		$updated_score[] = $_GET['projectId'];
		$updated_score[] = $_GET['teamId'];

		$sql = "UPDATE `project_team` SET `score` = ? WHERE projectId = ? AND teamId = ?";
		$stmt = $pdo->prepare($sql);

		$result = $stmt->execute($updated_score);

		if ($result) {
			$_SESSION['message']['type'] = 'success';
			$_SESSION['message']['content'] = 'La calificación se actualizó correctamente.';
			header('Location: set_score.php?projectId=' . $_GET['projectId'] . '&teamId=' . $_GET['teamId']);
			exit();
		} else {
			$_SESSION['message']['type'] = 'danger';
			$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
		}
	}
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Modificar Calificación</h1>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>

	<?php if (!empty($errors)): ?>
		<div class="alert danger">
			<span class="closebtn">&times;</span>
			<ul>
				<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
				<?php endforeach?>
			</ul>
		</div>
	<?php endif?>

	<form class="form" method="POST">
		<label for="score" class="label">Calificación:</label>
		<input name="score" id="score" type="text" class="input" value="<?php echo $project_team['score'] ?? 0 ?>" autofocus required>

		<input name="submit" type="submit" value="Guardar" class="button button--primary">
		<input type="reset" value="Cancelar" class="button">
	</form>
</main>
<?php include 'includes/footer.php';?>
</body>
</html>