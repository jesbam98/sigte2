<?php
/**
 * Detalles de un equipo
 */

require 'includes/checkauth.php';
require 'includes/config.php';
require 'includes/functions.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Revisar que sea proporcionado un id de equipo
if (empty($_GET['id'])) {
	include 'includes/404.php';
	exit();
} else {
	// Verificar que el equipo a editar exista
	$sql = "SELECT id FROM team WHERE id = ?";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['id']]);

	$result = $stmt->fetch();

	if ($result == false) {
		include 'includes/404.php';
		exit();
	}
}

// Título de la página
$page_title = 'Detalles de Equipo';

// Obtener los datos del equipo
$sql = "SELECT * FROM team WHERE id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$team = $stmt->fetch();

// Procesar formulario
if (!empty($_GET['action'])) {

	switch ($_GET['action']) {
	// Agregar un miembro
	case 'add_member':
		if (isset($_POST['submit'])) {
			$valid = true;

			if (empty($_POST['userId'])) {
				$valid = false;
			}

			if ($valid) {
				$sql = "INSERT INTO `team_member`(`userId`, `teamId`) VALUES (?, ?)";
				$stmt = $pdo->prepare($sql);
				$result = $stmt->execute([$_POST['userId'], $_GET['id']]);

				if ($result) {
					$_SESSION['message']['type'] = 'success';
					$_SESSION['message']['content'] = 'Miembro agregado correctamente.';
					header('Location: team.php?id=' . $_GET['id']);
					exit();
				} else {
					$_SESSION['message']['type'] = 'danger';
					$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
				}
			}
		}
		break;

	// Eliminar un miembro
	case 'delete_member':
		if (isset($_POST['submit'])) {
			$valid = true;

			if (empty($_POST['userId'])) {
				$valid = false;
			}

			if ($valid) {
				$sql = "DELETE FROM `team_member` WHERE userId = ? AND teamId = ?";
				$stmt = $pdo->prepare($sql);
				$deleted = $stmt->execute([$_POST['userId'], $_GET['id']]);

				$sql = "UPDATE `user` SET `permissions` = 0 WHERE id = ?";
				$stmt = $pdo->prepare($sql);
				$updated = $stmt->execute([$_POST['userId']]);

				if ($deleted && $updated) {
					$_SESSION['message']['type'] = 'success';
					$_SESSION['message']['content'] = 'Miembro eliminado correctamente.';
					header('Location: team.php?id=' . $_GET['id']);
					exit();
				} else {
					$_SESSION['message']['type'] = 'danger';
					$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
				}
			} else {
				$_SESSION['message']['type'] = 'danger';
				$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
			}
		}
		break;
	case 'set_leader':
		if (isset($_POST['submit'])) {
			$valid = true;

			if (empty($_POST['userId'])) {
				$valid = false;
			}

			if ($valid) {
				// Remover permisos al líder anterior
				$sql = "UPDATE `user` SET `permissions` = 0 WHERE `id` = ?";
				$stmt = $pdo->prepare($sql);
				$previous_leader_removed = $stmt->execute([$team['leaderId']]);

				// Establecer nuevo líder
				$sql = "UPDATE `team` SET `leaderId` = ? WHERE `id` = ?";
				$stmt = $pdo->prepare($sql);

				// Parámetos de la consulta
				$data = [];
				$data[] = $_POST['userId'];
				$data[] = $_GET['id'];

				$team_updated = $stmt->execute($data);

				$sql = "UPDATE `user` SET `permissions` = 1 WHERE `id` = ?";
				$stmt = $pdo->prepare($sql);

				$data = [];
				$data[] = $_POST['userId'];

				$user_updated = $stmt->execute($data);

				if ($previous_leader_removed && $team_updated && $user_updated) {
					$_SESSION['message']['type'] = 'success';
					$_SESSION['message']['content'] = 'Líder seleccionado correctamente.';
					header('Location: team.php?id=' . $_GET['id']);
					exit();
				} else {
					$_SESSION['message']['type'] = 'danger';
					$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
				}
			} else {
				$_SESSION['message']['type'] = 'danger';
				$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
			}
		}
		break;
	// Agregar un proyecto
	case 'add_project':
		if (isset($_POST['submit'])) {
			$valid = true;

			if (empty($_POST['projectId'])) {
				$valid = false;
			}

			if (empty($_POST['dueDate'])) {
				$valid = false;
			}

			if ($valid) {

				$sql = "INSERT INTO `project_team`(`projectId`, `teamId`, `dueDate`) VALUES(?, ?, ?)";
				$stmt = $pdo->prepare($sql);

				$data = [];
				$data[] = $_POST['projectId'];
				$data[] = $_GET['id'];
				$data[] = $_POST['dueDate'];

				$result = $stmt->execute($data);

				if ($result) {
					$_SESSION['message']['type'] = 'success';
					$_SESSION['message']['content'] = 'Proyecto asignado correctamente.';
					header('Location: team.php?id=' . $_GET['id']);
					exit();
				} else {
					$_SESSION['message']['type'] = 'danger';
					$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
				}
			} else {
				$_SESSION['message']['type'] = 'danger';
				$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
			}
		}
		break;
	// Eliminar un proyecto
	case 'delete_project':
		if (isset($_POST['submit'])) {

			$valid = true;

			if (empty($_POST['projectId'])) {
				$valid = false;
			}

			if ($valid) {

				$sql = "DELETE FROM project_team WHERE projectId = ? AND teamId = ?";
				$stmt = $pdo->prepare($sql);
				$data = [];
				$data[] = $_POST['projectId'];
				$data[] = $_GET['id'];
				$result = $stmt->execute($data);

				if ($result) {
					$_SESSION['message']['type'] = 'success';
					$_SESSION['message']['content'] = 'Proyecto eliminado correctamente.';
					header('Location: team.php?id=' . $_GET['id']);
					exit();
				} else {
					$_SESSION['message']['type'] = 'danger';
					$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
				}
			} else {
				$_SESSION['message']['type'] = 'danger';
				$_SESSION['message']['content'] = 'Ha ocurrido un problema.';
			}
		}
		break;
	default:
		break;
	}

}

// Obtener los miembros del equipo
$sql = "SELECT U.* FROM team_member AS TM JOIN user AS U ON TM.userId = U.id WHERE TM.teamId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$team_members = $stmt->fetchAll();

// Obtener los proyectos del equipo
$sql = "SELECT P.*, PT.* FROM project_team AS PT JOIN project AS P ON PT.projectId = P.id WHERE PT.teamId = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$assigned_projects = $stmt->fetchAll();

// Obtener los usuarios del tipo Alumno que no sean miembros de un equipo
$sql = "SELECT U.* FROM user AS U LEFT JOIN team_member AS TM ON U.id = TM.userId WHERE TM.userId IS NULL AND permissions = 0";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$users = $stmt->fetchAll();

// Obtener los proyectos que no hayan sido asignados
$sql = "SELECT P.* FROM project_team AS PT RIGHT JOIN project AS P ON PT.projectId = P.id WHERE PT.projectId IS NULL OR PT.projectId NOT IN (SELECT projectId FROM project_team WHERE teamId = ?) GROUP BY P.id";
$stmt = $pdo->prepare($sql);
$stmt->execute([$_GET['id']]);
$projects = $stmt->fetchAll();

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
	<link rel="stylesheet" href="assets/flatpickr/flatpickr.min.css">
	<script src="assets/flatpickr/flatpickr.js"></script>
	<script src="assets/flatpickr/es.js"></script>
	<style>
		.inlineForm {
			display: flex;
			align-items: center;
			margin-bottom: 1rem;
		}

		.inlineForm>* {
			margin: 0;
		}

		.inlineForm>*:not(:last-child) {
			margin-right: 1rem;
		}
	</style>
</head>
<body class="page">
	<?php include 'includes/navbar.php';?>
	<main class="page-content">
		<h1>Equipo: <?php echo $team['name'] ?></h1>

		<?php if (isset($_SESSION['message'])): ?>
			<div class="alert <?php echo $_SESSION['message']['type'] ?>">
				<span class="closebtn">&times;</span>
				<?php echo $_SESSION['message']['content'] ?>
			</div>
			<?php unset($_SESSION['message'])?>
		<?php endif?>

		<?php if (!empty($errors)): ?>
			<div class="alert danger">
				<span class="closebtn">&times;</span>
				<ul>
					<?php foreach ($errors as $error): ?>
						<li><?php echo $error ?></li>
					<?php endforeach?>
				</ul>
			</div>
		<?php endif?>


		<h3>Integrantes</h3>
		<form class="inlineForm" action="team.php?id=<?php echo escape($_GET['id']) ?>&action=add_member" method="POST">
			<select name="userId" id="user" class="select" required>
				<?php if ($users): ?>
					<?php foreach ($users as $row): ?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['firstName'] . ' ' . $row['lastName'] ?></option>
					<?php endforeach?>
				<?php else: ?>
						<option selected disabled>No se encontraron resultados</option>
				<?php endif?>
			</select>
			<input <?php echo !$users ? 'disabled' : '' ?> class="button button--secondary" name="submit" type="submit" value="Agregar">
		</form>

	<table class="table">
		<thead class="table-thead">
			<tr>
				<th>Nombre</th>
				<th>Líder</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($team_members): ?>
				<?php foreach ($team_members as $row): ?>
					<tr id="member<?php echo $row['id'] ?>">
						<td><?php echo $row['firstName'] . ' ' . $row['lastName'] ?></td>
						<td>
							<form method="POST" action="team.php?id=<?php echo $_GET['id'] ?>&action=set_leader">
								<input type="hidden" name="userId" value="<?php echo $row['id'] ?>" />
								<input name="submit" class="button <?php echo $team['leaderId'] == $row['id'] ? 'button--success' : '' ?>" type="submit" value="<?php echo $team['leaderId'] == $row['id'] ? 'Seleccionado' : 'Seleccionar' ?>" />
							</form>
						</td>
						<td>
							<form method="POST" action="team.php?id=<?php echo $_GET['id'] ?>&action=delete_member">
								<input type="hidden" name="userId" value="<?php echo $row['id'] ?>" />
								<input onclick="return confirm('¿Estás seguro que quieres eliminar este miembro?')" class="button button--danger" name="submit" type="submit" value="Eliminar" />
							</form>
						</td>
					</tr>
				<?php endforeach?>
			<?php else: ?>
				<tr>
					<td colspan="3" class="text--center">No se encontraron resultados.</td>
				</tr>
			<?php endif?>
		</tbody>
	</table>


		<h3>Proyectos</h3>
		<form class="inlineForm" action="team.php?id=<?php echo escape($_GET['id']) ?>&action=add_project" method="POST">
			<select name="projectId" id="user" class="select" required>
				<?php if ($projects): ?>
					<?php foreach ($projects as $row): ?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php endforeach?>
				<?php else: ?>
					<option selected disabled>No se encontraron resultados</option>
				<?php endif?>
			</select>
			<input type="text" name="dueDate" id="dueDate"  class="input datepicker" placeholder="Fecha de Entrega" required>
			<input <?php echo !$projects ? 'disabled' : '' ?> class="button button--secondary" name="submit" type="submit" value="Agregar">
		</form>

		<table class="table">
			<thead class="table-thead">
				<tr>
					<th>Nombre</th>
					<th>Fecha de Entrega</th>
					<th>Archivo Final</th>
					<th>Calificación</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($assigned_projects): ?>
					<?php foreach ($assigned_projects as $row): ?>
						<tr>
							<td><?php echo $row['name'] ?></td>
							<td><?php echo $row['dueDate'] ?></td>
							<td>
								<?php if ($row['finalFile']): ?>
									<a href="<?php echo UPLOADS_FOLDER . $row['finalFile'] ?>" download class="link">Descargar</a>
								<?php else: ?>
									No se ha subido el archivo final
								<?php endif?>
							</td>
							<td><?php echo $row['score'] . ' | ' ?> <a href="set_score.php?projectId=<?php echo $row['id'] ?>&teamId=<?php echo $team['id'] ?>" class="link">Modificar</a></td>
							<td>
								<form method="POST" action="team.php?id=<?php echo $_GET['id'] ?>&action=delete_project">
									<input type="hidden" name="projectId" value="<?php echo $row['id'] ?>" />
									<input onclick="return confirm('¿Estás seguro que quieres eliminar este proyecto?')" class="button button--danger" name="submit" type="submit" value="Eliminar" />
								</form>
							</td>
						</tr>
					<?php endforeach?>
				<?php else: ?>
					<tr>
						<td colspan="3" class="text--center">No se encontraron resultados.</td>
					</tr>
				<?php endif?>
			</tbody>
		</table>


	</main>
	<?php include 'includes/footer.php';?>
	<script>
		flatpickr('.datepicker', {
			locale: "es",
			minDate: "today"
		});
	</script>
</body>
</html>