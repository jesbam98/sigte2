<?php
/**
 * Listar equipos
 */
require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Título de la página
$page_title = 'Equipos';

// Registros por página
$limit = 5;

// Número de página
$page = 1;

// Revisar si se proporciona un número de página
if (isset($_GET['page'])) {
	// Validar que el número de página sea un número entero
	if (filter_var($_GET['page'], FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]])) {
		$page = $_GET['page'];
	}
}

// A partir de qué número de registro contar
$offset = ($page - 1) * $limit;

// Obtener los equipos
$sql = "SELECT * FROM team ORDER BY id DESC LIMIT $offset, $limit";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$results = $stmt->fetchAll();

// Obtener el total de equipos
$sql = "SELECT COUNT(*) FROM team";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$total_records = $stmt->fetchColumn(0);

// Calcular el total de páginas
$total_pages = ceil($total_records / $limit);
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Equipos</h1>

	<div class="actionBar">
		<a href="add_team.php" class="button button--secondary">Nuevo equipo</a>
	</div>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>


	<table class="table">
		<thead class="table-thead">
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($results): ?>
				<?php foreach ($results as $row): ?>
					<tr>
						<td><?php echo $row['id'] ?></td>
						<td><?php echo $row['name'] ?></td>
						<td><a class="link" href="team.php?id=<?php echo $row['id'] ?>">Detalles</a> | <a class="link" href="edit_team.php?id=<?php echo $row['id'] ?>">Editar</a> | <a class="link" href="delete_team.php?id=<?php echo $row['id'] ?>">Eliminar</a></td>
					</tr>
				<?php endforeach?>
			<?php else: ?>
				<tr>
					<td colspan="4" class="text--center">No se encontraron resultados</td>
				</tr>
			<?php endif?>
		</tbody>
	</table>

	 <?php if ($total_pages > 1): ?>
            <div class="pagination">
            <?php if ($page > 1) {
	echo '<a href="teams.php?page=' . ($page - 1) . '">&laquo;</a>';
}?>
            <?php for ($i = 1; $i <= $total_pages; $i++) {
	if ($i == $page) {
		echo "<a href=\"teams.php?page=$i\" class=\"active\">$i</a>";
	} else {
		echo "<a href=\"teams.php?page=$i\">$i</a>";
	}
}?>
        	<?php if ($page < $total_pages) {
	echo '<a href="teams.php?page=' . ($page + 1) . '">&raquo;</a>';
}?>
            </div>
        <?php endif?>
<?php include 'includes/footer.php';?>
</main>
</body>
</html>