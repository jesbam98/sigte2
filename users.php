<?php
/**
 * Listar usuarios
 */
require 'includes/checkauth.php';
require 'includes/config.php';

if ($_SESSION['user']['permissions'] != 2) {
	include 'includes/403.php';
	exit();
}

// Título de la página
$page_title = 'Usuarios';

// Registros por página
$limit = 5;

// Número de página
$page = 1;

// Revisar si se proporciona un número de página
if (isset($_GET['page'])) {
	// Validar que el número de página sea un número entero
	if (filter_var($_GET['page'], FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]])) {
		$page = $_GET['page'];
	}
}

// A partir de qué número de registro contar
$offset = ($page - 1) * $limit;

// Obtener los usuarios
$sql = "SELECT * FROM user ORDER BY createdAt DESC LIMIT $offset, $limit";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$results = $stmt->fetchAll();

// Obtener el total de usuarios
$sql = "SELECT COUNT(*) FROM user";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$total_records = $stmt->fetchColumn(0);

// Calcular el total de páginas
$total_pages = ceil($total_records / $limit);
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include 'includes/header.php';?>
</head>
<body class="page">
<?php include 'includes/navbar.php';?>
<main class="page-content">
	<h1>Usuarios</h1>

	<div class="actionBar">
		<a href="add_user.php" class="button button--secondary">Nuevo usuario</a>
	</div>

	<?php if (isset($_SESSION['message'])): ?>
		<div class="alert <?php echo $_SESSION['message']['type'] ?>">
			<span class="closebtn">&times;</span>
			<?php echo $_SESSION['message']['content'] ?>
		</div>
		<?php unset($_SESSION['message'])?>
	<?php endif?>


	<table class="table">
		<thead class="table-thead">
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Apellidos</th>
				<th>Correo</th>
				<th>Usuario</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($results): ?>
				<?php foreach ($results as $row): ?>
					<tr>
						<td><?php echo $row['id'] ?></td>
						<td><?php echo $row['firstName'] ?></td>
						<td><?php echo $row['lastName'] ?></td>
						<td><?php echo $row['email'] ?></td>
						<td><?php echo $row['username'] ?></td>
						<td><a class="link" href="edit_user.php?id=<?php echo $row['id'] ?>">Editar</a> | <a class="link" href="delete_user.php?id=<?php echo $row['id'] ?>">Eliminar</a></td>
					</tr>
				<?php endforeach?>
			<?php else: ?>
				<tr>
					<td colspan="5" class="text--center">No se encontraron resultados</td>
				</tr>
			<?php endif?>
		</tbody>
	</table>

	 <?php if ($total_pages > 1): ?>
            <div class="pagination">
            <?php if ($page > 1) {
	echo '<a href="users.php?page=' . ($page - 1) . '">&laquo;</a>';
}?>
            <?php for ($i = 1; $i <= $total_pages; $i++) {
	if ($i == $page) {
		echo "<a href=\"users.php?page=$i\" class=\"active\">$i</a>";
	} else {
		echo "<a href=\"users.php?page=$i\">$i</a>";
	}
}?>
        	<?php if ($page < $total_pages) {
	echo '<a href="users.php?page=' . ($page + 1) . '">&raquo;</a>';
}?>
            </div>
        <?php endif?>
<?php include 'includes/footer.php';?>
</main>
</body>
</html>